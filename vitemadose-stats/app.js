const got = require('got');
const fs = require('fs');
const fsp = require('fs').promises;

async function verif() {
    try {
        var response = await got('https://vitemadose.gitlab.io/vitemadose/stats.json');
        var statsData = JSON.parse(response.body);
        console.log(statsData);
    } catch (error) {
        console.log(error);
    }
    var statsTotal = {
        center: {
            "total": 0
        },
        appointment_count: {
            "total": 0
        }
    }
    for (var s in statsData) {
        if (s == "tout_departement" || s == "om") {
            continue;
        }
        console.log(s);
        try {
            var response = await got('https://vitemadose.gitlab.io/vitemadose/' + s + '.json');
            var depData = JSON.parse(response.body);
            var centers = depData["centres_disponibles"].concat(depData["centres_indisponibles"]);
            for (var c in centers) {
                var center = centers[c];
                if (!statsData[s]["center"]) {
                    statsData[s]["center"] = {};
                }
                if (statsData[s]["center"][center["plateforme"]]) {
                    statsData[s]["center"][center["plateforme"]]++;
                } else {
                    statsData[s]["center"][center["plateforme"]] = 1;
                }

                if (statsTotal["center"][center["plateforme"]]) {
                    statsTotal["center"][center["plateforme"]]++;
                } else {
                    statsTotal["center"][center["plateforme"]] = 1;
                }
                statsTotal["center"]["total"]++;

                if (!statsData[s]["appointment_count"]) {
                    statsData[s]["appointment_count"] = {};
                }
                if (statsData[s]["appointment_count"][center["plateforme"]] != undefined) {
                    statsData[s]["appointment_count"][center["plateforme"]] += (center["appointment_count"] ? center["appointment_count"] : 0);
                } else {
                    statsData[s]["appointment_count"][center["plateforme"]] = (center["appointment_count"] ? center["appointment_count"] : 0);
                }

                if (statsTotal["appointment_count"][center["plateforme"]] != undefined) {
                    statsTotal["appointment_count"][center["plateforme"]] += (center["appointment_count"] ? center["appointment_count"] : 0);
                } else {
                    statsTotal["appointment_count"][center["plateforme"]] = (center["appointment_count"] ? center["appointment_count"] : 0);
                }
                statsTotal["appointment_count"]["total"] += (center["appointment_count"] ? center["appointment_count"] : 0);
            }
        } catch (error) {
            console.log(error);
        }
    }
    for (var s in statsTotal) {
        statsTotal[s+"_%"] = {};
        for (var x in statsTotal[s]) {
            if(x == 'total'){
                continue;
            }
            statsTotal[s+"_%"][x] = Number.parseFloat(statsTotal[s][x]/statsTotal[s]['total']*100).toFixed(2);
        }
    }
    console.log(statsData);
    await fsp.writeFile('statsData.json', JSON.stringify(statsData));
    console.log(statsTotal);
    await fsp.writeFile('statsTotal.json', JSON.stringify(statsTotal));
};
verif();