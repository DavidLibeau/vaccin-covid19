var pattern = "https://events.data.doctolib.com/live";

function cancel(requestDetails) {
    console.log("Canceling: " + requestDetails.url);
    console.log(requestDetails);
    var data = arrayBufferToJson(requestDetails.requestBody.raw[0].bytes)
    console.log(window.atob(data["Record"]["Data"]));
    browser.tabs.query({
        currentWindow: true,
        active: true
    }).then(function (tabs) {
        sendMessageToTabs(tabs, window.atob(data["Record"]["Data"]))
    }).catch(onError);
    return {
        cancel: true
    };
}

browser.webRequest.onBeforeRequest.addListener(
    cancel, {
        urls: [pattern]
    },
  ["blocking", "requestBody"]
);


function arrayBufferToJson(buffer) {
    var binary = '';
    var bytes = new Uint8Array(buffer);
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i]);
    }
    return JSON.parse(binary);
}

function sendMessageToTabs(tabs, data) {
    for (let tab of tabs) {
        browser.tabs.sendMessage(
            tab.id, {
                data: data
            }
        ).then(response => {
            console.log("Message from the content script:");
            console.log(response.response);
        }).catch(onError);
    }
}

function onError(error) {
  console.error(`Error: ${error}`);
}

console.log("Doctolib tracking background script loaded");