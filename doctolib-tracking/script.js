"use strict";

browser.runtime.onMessage.addListener(request => {
    console.log("Message from the background script:");
    console.log(request.data);
    console.log($("#doctolibTracking").length);
    if (!$("#doctolibTracking").length) {
        $("head").append('<style>#doctolibTracking { position: fixed; bottom: 10px; right: 10px; width: 500px; max-width: 50vw; height: 300px; max-height: 50vh; background-color: #e8e8e8; border-radius: 10px; border: 5px solid #c13e3e; z-index: 999;} #doctolibTracking h4 {margin:0; padding: 10px; border-bottom: 5px solid #c13e3e;} #doctolibTracking>div { overflow-x: auto; position: absolute; bottom: 0; top: 43px; left: 5px; right: 5px;} #doctolibTracking ul { list-style: none; padding: 0;} #doctolibTracking>div>ul>li { background-color: white; border-radius: 5px; padding: 5px; margin-bottom: 5px;} #doctolibTracking>div>ul>li:first-child { margin-top: 5px;}</style>');
        $("body").append('<div id="doctolibTracking"><h4>Pistage de Doctolib intercepté</h4><div><ul></ul></div></div>');
    }
    if ($("#doctolibTracking").length) {
        console.log("output");
        var split = request.data.split("\n");
        for (var s in split) {
            var data = JSON.parse(split[s]);
            console.log(data);
            var date = new Date();
            var html = "<p>" + date.toString() + "</p>";
            $.each(data, function (index, item) {
                html += "<ul><strong>" + index + ":</strong> ";
                if (typeof item === 'object') {
                    html += '<ul style="margin-left:5px;">';
                    $.each(item, function (indexChild, itemChild) {
                        html += "<li><strong>" + indexChild + ":</strong> " + itemChild + "</li>";
                    });
                    html += "</ul>";
                } else {
                    html += item;
                }
                html += "</ul>";
            });
            $("#doctolibTracking>div>ul").append('<li>' + html + '</li>')
        }
    }
});

console.log("Doctolib tracking extension loaded");