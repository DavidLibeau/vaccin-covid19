require('dotenv').config();
const parseCSV = require('csv-parse/lib/sync');
const stringifyCSV = require('csv-stringify')
const assert = require('assert');
const got = require('got');
const fs = require('fs');
const fsp = require('fs').promises;
const mkdirp = require('mkdirp')
const FormData = require('form-data');
const getStream = require("get-stream");
const moment = require('moment');
const parseXML = require('xml2js').parseString;

async function scrapDoctolibFrance() {
    console.log("Scraping Doctolib France (" + moment().format('DD/MM/YYYY-HH:mm') + ")");
    var franceData = [];
    var page = 0;
    do {
        try {
            const response = await got('https://www.doctolib.fr/vaccination-covid-19/france.json?page=' + page);
            var data = JSON.parse(response.body);
            data = data.data;
            console.log(data);
            if (data.doctors.length) {
                franceData = franceData.concat(data.doctors);
                await fsp.writeFile('doctolib-france.json', JSON.stringify(franceData));
            }
            page++;
        } catch (error) {
            console.log(error);
        }
    } while (data.doctors.length != 0)
}


async function scrapDoctolibCenter(center, cDone) {
    console.log("(" + Math.round(cDone / (centers.length - 1) * 100) + "%) Scraping Doctolib center : " + center.name_with_title + " (" + moment().format('DD/MM/YYYY-HH:mm') + ")");
    try {
        var response = await got('https://partners.doctolib.fr/booking/' + center.link.split('/')[center.link.split('/').length - 1] + '.json');
        var centerData = JSON.parse(response.body);
        centerData = centerData.data;
    } catch (error) {
        console.log('Error: https://partners.doctolib.fr/booking/' + center.link.split('/')[center.link.split('/').length - 1] + '.json');
        console.log(error);
    }
    var visit_motive = {};
    for (var v in centerData["visit_motives"]) {
        if (centerData["visit_motives"][v]["name"].indexOf("vaccin COVID-19") != -1) {
            visit_motive[centerData["visit_motives"][v]["id"]] = centerData["visit_motives"][v];
        }
    }
    if (Object.keys(visit_motive).length == 0) {
        console.log("No visit_motive_id for " + 'https://partners.doctolib.fr/booking/' + center.link.split('/')[center.link.split('/').length - 1] + '.json');
        globalMetadata['errors']['visit_motive_id_not_found'].push('https://partners.doctolib.fr/booking/' + center.link.split('/')[center.link.split('/').length - 1] + '.json');
        return undefined;
    }
    var data = {
        'metadata': centerData,
        'availabilities': {}
    }
    for (var visit_motive_id in visit_motive) {
        try {
            var response = await got("https://www.doctolib.fr/availabilities.json?start_date=" + moment().format('YYYY-MM-DD') + "&visit_motive_ids=" + visit_motive_id + "&agenda_ids=" + centerData["agendas"][0]["id"] + "&practice_ids=" + centerData["places"][0]["practice_ids"][0]);
            var availabilitiesData = JSON.parse(response.body);
            data['availabilities'][visit_motive[visit_motive_id]['name']] = {
                metadata: visit_motive[visit_motive_id],
                data: availabilitiesData
            };
        } catch (error) {
            console.log('https://partners.doctolib.fr/booking/' + center.link.split('/')[center.link.split('/').length - 1] + '.json');
            console.log("https://www.doctolib.fr/availabilities.json?start_date=" + moment().format('YYYY-MM-DD') + "&visit_motive_ids=" + visit_motive_id + "&agenda_ids=" + centerData["agendas"][0]["id"] + "&practice_ids=" + centerData["places"][0]["practice_ids"][0]);
        }

    }
    return data;

}
async function scrapDoctolibCenters() {
    var dir = await fsp.readdir('doctolib-vaccination');
    console.log(dir);
    /*for (var d in dir) {
        if (dir[d][0] != '.') {
            await fsp.writeFile('doctolib-vaccination/' + dir[d], '{}');
        }
    }*/
    centers = await fsp.readFile('doctolib-france.json');
    centers = JSON.parse(centers);
    var database = {};
    var cStarted = -1;
    var cDone = -1;
    var scrapping = true;
    for (var c in centers) {
        setTimeout(async function () {
            cStarted++;
            var cLocal = cStarted;
            //console.log(cLocal);
            //console.log(centers[cLocal].id);
            if (database[centers[cStarted].id]) {
                //console.log('Duplicate (' + centers[cLocal].id + ')');
            } else {
                database[centers[cLocal].id] = 1;
                var zipcode = centers[cLocal].zipcode.replace(" Cedex", "");
                if (zipcode.length > 5 && zipcode[0] != 9) {
                    zipcode = zipcode.slice(0, 5);
                }
                var dep = zipcode.slice(0, -3);
                if (dep.length == 1) {
                    dep = "0" + dep;
                }
                var centerData = await scrapDoctolibCenter(centers[cLocal], cDone);
                if (centerData) {
                    await mkdirp('doctolib-vaccination/' + dep);
                    await fsp.writeFile('doctolib-vaccination/' + dep + '/' + centers[cLocal].id + '.json', JSON.stringify({
                        'metadata': centers[cLocal],
                        'data': centerData
                    }));
                }
            }
            cDone++;
            checkIfScrapingFinished(cDone, globalMetadata);
        }, c * 100);
        c++;
    }
    console.log(c);
    console.log("Launching scrap!");
    globalMetadata = {
        'last_update': moment().format(),
        'errors': {
            'visit_motive_id_not_found': []
        },
        'dir': {

        }
    };
}


async function pushToDataGouv(fileName, datasetId, ressourceId) {
    try {
        const stream = fs.createReadStream(fileName);
        stream.on('data', chunk => console.log(`(${fileName}) Uploading ${chunk.length} bytes...`));

        var form = new FormData();
        form.append('file', stream);
        form.append("type", "application/json");

        const response = await got.post('https://www.data.gouv.fr/api/1/datasets/' + datasetId + '/resources/' + ressourceId + '/upload/', {
            responseType: 'json',
            headers: {
                'X-Api-Key': process.env.API_KEY,
                "Accept": "application/json",
                'user-agent': "David Libeau (dav.li)"
            },
            body: form
        }).then(res => {
            console.log(res);
            console.log(fileName + ' updated!');
        }).catch(function (error) {
            console.log(error);
            console.log(error.response.headers);
            console.log(error.response.body);
        });
    } catch (error) {
        console.log(error);
    }
    return;
}

async function checkIfScrapingFinished(cDone, globalMetadata) {
    if (centers.length - 1 == cDone) {
        console.log("(100%) Done.");
        await fsp.writeFile('metadata.json', JSON.stringify(globalMetadata));
        await pushToDataGouv("metadata.json", datasetId, '5c7e5503-9097-4e73-a23d-184a53401922');
        var dir = await fsp.readdir('doctolib-vaccination');
        console.log(dir);
        var response = await got("https://www.data.gouv.fr/api/1/datasets/" + datasetId);
        response = JSON.parse(response.body);
        for (var d in dir) {
            if (dir[d][0] != '.') {
                for (var r in response['resources']) {
                    if ('doctolib-vaccination-'+dir[d]+'.json' == response['resources'][r]['title']) {
                        var dirDep = await fsp.readdir('doctolib-vaccination/' + dir[d]);
                        console.log(dirDep);
                        var depData = [];
                        for (var dd in dirDep) {
                            if (dirDep[dd][0] != '.') {
                                depData.push(JSON.parse(await fsp.readFile('doctolib-vaccination/' + dir[d] + '/' +dirDep[dd])));
                            }
                        }
                        await fsp.writeFile('doctolib-vaccination/doctolib-vaccination-' + dir[d]+'.json',JSON.stringify(depData));
                        await pushToDataGouv('doctolib-vaccination/doctolib-vaccination-' + dir[d]+'.json', datasetId, response['resources'][r]['id']);
                    }
                }
            }
        }
    }
}

var datasetId = "609b824b1beba03274c157e2";
var centers = {};
var globalMetadata = {};

setInterval(scrapDoctolibCenters, 30 * 60 * 60 * 1000);
scrapDoctolibCenters();
